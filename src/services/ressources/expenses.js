import httpClient from "./http-client";

export default {
  getExpenses() {
    return httpClient.get("/depenses");
  },

  getExpense(id) {
    return httpClient.get(`/depenses/${id}`);
  },
};
