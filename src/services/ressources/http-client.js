import store from "@/store";
import axios from "axios";

const httpClient = axios.create({
  baseURL: "http://localhost:3000",
  headers: { "Content-Type": "application/json", Accept: "application/json" },
  timeout: 2500,
});

httpClient.interceptors.response.use(
  (data) => {
    store.dispatch("stopLoading");
    return data;
  },
  () => {
    store.dispatch("stopLoading");
  }
);

export default httpClient;
