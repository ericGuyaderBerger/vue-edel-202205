import Vue from "vue";
import VueRouter from "vue-router";
// import store from "@/store";

import HomeView from "../views/HomeView.vue";
import ExpenseDetailsView from "../views/ExpenseDetailsView.vue";
import NotFoundView from "../views/NotFoundView.vue";
import ExpenseEdit from "@/components/expense/ExpenseEdit.vue";
import ExpenseClose from "@/components/expense/ExpenseClose.vue";
import ExpenseDetails from "@/components/expense/ExpenseDetails.vue";
import store from "@/store";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "home",
    component: HomeView,
    meta: { useApi: true },
  },
  {
    path: "/about-us",
    name: "about",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/AboutView.vue"),
  },
  {
    path: "/about",
    redirect: "/about-us",
  },
  {
    path: "/expense/:id",
    name: "expense-details-view",
    component: ExpenseDetailsView,
    props: true,
    meta: { useApi: true },
    children: [
      {
        path: "",
        name: "expense-details",
        component: ExpenseDetails,
      },
      {
        path: "edit",
        name: "expense-edit",
        component: ExpenseEdit,
      },
      {
        path: "close",
        name: "expense-close",
        component: ExpenseClose,
      },
    ],
  },
  {
    path: "/:catch(.*)",
    name: "not-found",
    component: NotFoundView,
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach((to, from, next) => {
  if (to.meta && to.meta.useApi) {
    store.dispatch("startLoading");
  }
  next();
});

export default router;
