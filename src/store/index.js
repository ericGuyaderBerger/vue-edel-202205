import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    errorMessage: "",
    loading: false,
  },
  getters: {},
  mutations: {
    SET_ERROR_MESSAGE(state, payload) {
      state.errorMessage = payload;
    },
    TOGGLE_LOADING(state) {
      state.loading = !state.loading;
    },
    SET_LOADING(state, payload) {
      state.loading = payload;
    },
  },
  actions: {
    changeErrorMessage({ commit }, payload) {
      commit("SET_ERROR_MESSAGE", payload);
    },
    toggleLoading({ commit }) {
      commit("TOGGLE_LOADING");
    },
    startLoading({ commit }) {
      commit("SET_LOADING", true);
    },
    stopLoading({ commit }) {
      commit("SET_LOADING", false);
    },
  },
  modules: {},
});
