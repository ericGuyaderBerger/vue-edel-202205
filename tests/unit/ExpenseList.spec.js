import { mount } from "@vue/test-utils";
import ExpenseList from "@/components/expense/ExpenseList.vue";
import ExpenseService from "@/services/ressources/expenses";
import flushPromises from "flush-promises";

jest.mock("@/services/ressources/expenses");

beforeEach(() => {
  jest.clearAllMocks();
});

describe("ExpenseList", () => {
  it("renders a no data message when no expenses are fulfilled", () => {
    const wrapper = mount(ExpenseList);
    expect(wrapper.find("[data-testid=no-expenses]")).toBeDefined();
  });

  it("calls getExpenses one time", () => {
    //ExpenseService.getExpenses.mockResolvedValueOnce({ data: [] });
    mount(ExpenseList);
    expect(ExpenseService.getExpenses).toHaveBeenCalledTimes(1);
  });

  it("renders one list item per expense", async () => {
    ExpenseService.getExpenses.mockResolvedValueOnce({
      data: [{ a: 1 }, { a: 2 }, { a: 3 }],
    });

    const wrapper = mount(ExpenseList, {
      stubs: {
        ExpenseListItem: { template: "<li>a</li>" },
      },
    });
    await flushPromises();
    //console.log(wrapper.html());
    expect(wrapper.findAll("li")).toHaveLength(3);
    //   //expect(true).toBe(true);
  });
});
