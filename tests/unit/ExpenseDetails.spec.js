import { mount } from "@vue/test-utils";
import ExpenseDetails from "@/components/expense/ExpenseDetails.vue";

describe("ExpenseDetails", () => {
  it("renders nothing when expense is null", () => {
    const wrapper = mount(ExpenseDetails, {
      propsData: {
        expense: null,
      },
    });
    expect(wrapper.html()).toEqual("");
  });

  it("renders expense information when valid expense is passed ", () => {
    const wrapper = mount(ExpenseDetails, {
      propsData: {
        expense: {
          depense: 38,
          nature: "nature dépense 1",
        },
      },
    });
    expect(wrapper.find("[data-testid=depense]").text()).toEqual("38");
    expect(wrapper.find("[data-testid=nature]").text()).toEqual(
      "nature dépense 1"
    );
  });
});
